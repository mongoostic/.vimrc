"=============================================================================
" Description: Vim .vimrc
" Author: Anton Tiutiunnikov <mongoostic@gmail.com>
" URL: http://yabp.ru
"=============================================================================

" Before the first start run next command for install Vundle plugin manager:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/fzf
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" List your plugins here:
        " provides some utility functions and commands for programming in Vim
        Bundle 'L9'
    " Interface
        " Browsing and find files, buffers ...
        Bundle 'git://github.com/ctrlpvim/ctrlp.vim.git'
        Bundle 'https://github.com/junegunn/fzf.vim.git'
        Bundle 'junegunn/fzf'
        Bundle 'mileszs/ack.vim'
        Bundle 'https://github.com/jremmen/vim-ripgrep.git'
        Bundle 'https://github.com/sk1418/QFGrep.git'
        " A tree explorer plugin
        Bundle 'git://github.com/scrooloose/nerdtree.git'
        " autoupdate ctags
        " Bundle 'git://github.com/craigemery/vim-autotag.git'
        " Bundle 'https://github.com/c0r73x/neotags.nvim.git'

        " vim-gutentags
        " for good work need install universal ctags
        Bundle 'https://github.com/ludovicchabant/vim-gutentags.git'
        " Shows 'Nth match out of M' at every search
        Bundle 'git://github.com/vim-scripts/IndexedSearch.git'
        Bundle 'https://github.com/ggvgc/vim-fuzzysearch.git'
	" instead zencoding now using emmet-vim
	Bundle 'git://github.com/mattn/emmet-vim.git'
    " Provides easy code commenting
    " Bundle 'https://github.com/preservim/nerdcommenter.git'
    " Bundle 'git://github.com/tomtom/tcomment_vim.git'
        " Mappings to easily delete, change and add surroundings in pairs
    Bundle 'https://github.com/tpope/vim-commentary.git'
	Bundle 'git://github.com/tpope/vim-surround.git'
    Bundle 'git://github.com/tpope/vim-dispatch.git'
    Bundle 'git://github.com/tpope/vim-repeat.git'
    " Git wrapper
    Bundle 'git://github.com/tpope/vim-fugitive.git'
    Bundle 'git://github.com/airblade/vim-gitgutter.git'
    " Bundle 'https://github.com/tveskag/nvim-blame-line.git'
    " Align text
    " Bundle 'git://github.com/tsaleh/vim-align.git'
    Bundle 'https://github.com/junegunn/vim-easy-align.git'
    " Tabular
    Bundle 'https://github.com/godlygeek/tabular.git'
    " Buffer explorer
    Bundle 'git://github.com/jlanzarotta/bufexplorer.git'
    "Bundle 'https://github.com/fholgado/minibufexpl.vim.git'
    Bundle 'https://github.com/qpkorr/vim-bufkill.git'
    "Bundle 'https://github.com/jeetsukumaran/vim-buffergator.git'
    "deprecated
    "Bundle 'https://github.com/Shougo/unite.vim.git'
    "Bundle 'https://github.com/Shougo/denite.nvim.git'
        " Undo manager
        Bundle 'git://github.com/mbbill/undotree.git'
	" Yank Ring solutions (different)
        " Yank Ring may be, but has keybinding conflict with ctrlp
        " Bundle 'YankRing.vim'
        " Alternate YankRing
        Bundle "https://github.com/maxbrunsfeld/vim-yankstack.git"
        " Bundle "https://github.com/svermeulen/vim-yoink.git"
        " Bundle "https://github.com/svermeulen/vim-subversive.git"

        " Vim Viki in Text files
        Bundle 'git://github.com/vim-scripts/vimwiki.git'
        " Good check syntax for all, but slowly for Perl PerlCritic, need adjust somehow
        "Bundle 'git://github.com/scrooloose/syntastic.git'
        " Ale check syntax (async)
        Plugin 'dense-analysis/ale'
        " Work with TODO, FIXME and over, use <leader>t
        Bundle 'git://github.com/vim-scripts/TaskList.vim.git'
        Plugin 'aserebryakov/vim-todo-lists'
        " vim-airline - Lean & mean status/tabline for vim
    Bundle 'https://github.com/vim-airline/vim-airline'
    Bundle 'git://github.com/preservim/tagbar.git'
        " vim-airline themes
    " Bundle 'vim-airline/vim-airline-themes'
        " Work with sessions
    Bundle 'git://github.com/xolox/vim-session.git'
    "Bundle 'https://github.com/tpope/vim-obsession.git'
        " depends for vim-session
    Bundle 'git://github.com/xolox/vim-misc.git'
        " vim-tmux-navigator
    Bundle 'christoomey/vim-tmux-navigator'
    " Vim multiple cursors!
    " vim-multiple-cursors is not work in neovim!
    " Bundle 'git://github.com/terryma/vim-multiple-cursors.git'
    Bundle 'git://github.com/Yggdroot/indentLine.git'

    " Плагин, чтобы показывать indent полосу, попробовать
    " https://github.com/nathanaelkane/vim-indent-guides.git

    Bundle 'https://github.com/mg979/vim-visual-multi.git'
    " vim -autocomplete
        " Bundle 'git://github.com/Valloric/YouCompleteMe.git'
        Bundle 'https://github.com/neoclide/coc.nvim.git'
        " Command Occur for open quickfix window with all founded patterns in active buffer
        Bundle 'occur.vim'

    " Web

    " HTML/HAML
        " Matchit for tags
        " Failure on class="" in tag's
        "Bundle 'git://github.com/gregsexton/MatchTag.git'
         Bundle 'Valloric/MatchTagAlways'
    " JavaScript
        " Vastly improved vim's javascript indentation
        Bundle 'git://github.com/pangloss/vim-javascript.git'
        " Syntax for jQuery keywords and css selectors
        Bundle 'git://github.com/itspriddle/vim-jquery.git'
        Bundle 'git://github.com/MaxMEllon/vim-jsx-pretty.git'
        Bundle 'https://github.com/prettier/vim-prettier.git'
    "CSS
        Bundle 'git://github.com/ap/vim-css-color.git'
        " Syntax Vue
        Bundle 'git://github.com/posva/vim-vue.git'
        Bundle 'git://github.com/tyru/caw.vim.git'
        Bundle 'git://github.com/csscomb/vim-csscomb.git'

    "Puppet
        " Bundle 'git://github.com/rodjek/vim-puppet.git'

    " Snippets
    Bundle 'SirVer/ultisnips'

    " ReactJs
    Bundle 'mlaursen/vim-react-snippets'

    " JSON
    " Syntax highlighting for JSON in Vim
        Bundle 'git://github.com/leshill/vim-json.git'
    " Perl
        Bundle 'git://github.com/vim-perl/vim-perl.git'
    " Support syntax for mojolicious
    Bundle 'git://github.com/yko/mojo.vim.git'
    " Perl IDE -- Write and run Perl-scripts using menus and hotkeys
    " Bundle 'perl-support.vim'
    " Systemd
        Bundle 'https://github.com/kurayama/systemd-vim-syntax.git'
    " DB Access
    Bundle 'dbext.vim'
    " Markdown
    "Plugin 'godlygeek/tabular'
    "Plugin 'plasticboy/vim-markdown'
    " Plugin 'suan/vim-instant-markdown'
    "Plugin 'JamshedVesuna/vim-markdown-preview'
    Plugin 'shime/vim-livedown'
    "Bundle 'https://github.com/jaxbot/browserlink.vim.git'
    " Bundle 'https://github.com/prettier/vim-prettier.git'
    " Highlight syntax pug
    Plugin 'https://github.com/gu-fan/riv.vim.git'
    Plugin 'Rykka/InstantRst'

    Bundle 'git://github.com/digitaltoad/vim-pug.git'
    Bundle 'git://github.com/vim-scripts/SQLUtilities.git'
    Bundle 'git://github.com/kshenoy/vim-signature.git'
    " Bundle 'git://github.com/ronakg/quickr-preview.vim.git'
    Bundle 'https://github.com/vim-scripts/DrawIt.git'
    Bundle 'https://github.com/gyim/vim-boxdraw.git'
    Bundle 'https://github.com/dhruvasagar/vim-table-mode.git'
    " Python plugins
        Bundle 'git://github.com/Vimjas/vim-python-pep8-indent.git'
        Plugin 'https://github.com/davidhalter/jedi-vim.git'
        Plugin 'vim-python/python-syntax'
        "
        " Не понятно, что вообще делает python-mode
        " Plugin 'python-mode/python-mode'
        " Bundle 'https://github.com/python-mode/python-mode.git'
    " Плагин для переименовывания табов
    Bundle 'https://github.com/gcmt/taboo.vim.git'
    " Плагин позволяет задать для таба рабочую директорию, становится удобно
    " работать с несколькими проектами (микросервисами), совместно с NERDTree
    " и CtrlP
    " Не забудьте что для установки рабочей директории нужно прописать в
    " каждом проекте файл настроек .local.vimrc как минимум с одной строкой
    " let t:wd = "/path/to/the/project"
    "Bundle 'https://github.com/vim-scripts/tcd.vim.git'
    Bundle 'https://github.com/thinca/vim-localrc.git'
    " Vim debug
    "Bundle 'https://github.com/vim-vdebug/vdebug.git'
    " Иконки в файловом менеджере
    Bundle 'https://github.com/ryanoasis/vim-devicons.git'
    " Сортировка параметров в функции g>, g<, gs
    Bundle 'https://github.com/machakann/vim-swap.git'
    " Подключает API более высокого уровня к встроенному терминалу neo/vim. Например,
    " :T {text} отправляет {text} в консоль
    " Bundle 'https://github.com/kassio/neoterm.git'
    " Замена для Explore
    "Bundle 'https://github.com/tpope/vim-vinegar.git'
    Bundle 'https://github.com/mhinz/vim-startify.git'
    Bundle 'https://github.com/skywind3000/vim-quickui.git'
    Bundle 'skywind3000/asyncrun.vim'
    " Popup (для neovim, в vim работает из коробки)
    "Bundle 'https://github.com/ncm2/float-preview.nvim.git'
    "Bundle 'https://github.com/bfrg/vim-qf-preview.git'
    " Плагин для скрытия|показа folded линий
    Bundle 'showhide.vim'
    Bundle 'https://github.com/tpope/vim-unimpaired.git'
    " Bundle 'https://github.com/xolox/vim-colorscheme-switcher.git'
    " Bundle 'https://github.com/altercation/vim-colors-solarized.git'
    "
    " Colorschemes
    Plugin 'morhetz/gruvbox'
    Plugin 'lifepillar/vim-solarized8'
    Plugin 'junegunn/seoul256.vim'
    Plugin 'w0ng/vim-hybrid'
    " Bundle 'ayu-theme/ayu-vim'
    " Bundle 'https://github.com/rakr/vim-one.git'
    " *********************************
    " Выравнивание аргументов для методов и функций
    " Bundle 'https://github.com/AndrewRadev/splitjoin.vim.git'
    Bundle 'https://github.com/FooSoft/vim-argwrap.git'

    Bundle 'https://github.com/markonm/traces.vim.git'
    Plugin 'cespare/vim-toml'

    " Plugin 'sheerun/vim-polyglot'

    " Templates syntax
    Bundle 'https://github.com/Glench/Vim-Jinja2-Syntax.git'

    " Diagrams
    Bundle 'https://github.com/aklt/plantuml-syntax.git'
    Bundle 'https://github.com/weirongxu/plantuml-previewer.vim.git'

    " Miscellaneous
    Bundle 'https://github.com/tyru/open-browser.vim.git'
	Bundle 'https://github.com/michaeljsmith/vim-indent-object'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype indent on    " required
filetype plugin on    " required
" To ignore plugin indent changes, instead use:
" filetype indent on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
"

" Interface
    " Character encoding used inside Vim
    " Only available when compiled with the +multi_byte feature
    set encoding=utf-8
    " Character encodings considered when starting to edit an existing file
    " Only available when compiled with the +multi_byte feature
    set fileencodings=utf-8,cp1251
    " Always add lf in the end of file
    set fileformat=unix
    " Enhance command-line completion
    " Only available when compiled with the +wildmenu feature
    set wildmenu
    " Set completion mode
    " When more than one match, list all matches and complete first match
    " Then complete the next full match
    set wildmode=list:longest,full
    " Ignore following files when completing file/directory names
    " Version control
    set wildignore+=.hg,.git,.svn
    " OS X
    set wildignore+=*.DS_Store
    " Python byte code
    set wildignore+=*.pyc
    " Binary images
    set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg
    " Set title of the window to filename [+=-] (path) - VIM
    " Only available when compiled with the +title feature
    set title
    " Show (partial) command in the last line of the screen
    " Comment this line if your terminal is slow
    " Only available when compiled with the +cmdline_info feature
    set showcmd

    " Wrap long lines
        set wrap
    " Don't break words when wrapping
    " Only available when compiled with the +linebreak feature
    set linebreak
    " Show ↪ at the beginning of wrapped lines
    if has("linebreak")
        let &sbr = nr2char(8618).' '
    endif
    " Number of column to be highlighted
    " Only available when compiled with the +syntax feature
    if version >= 703
        set colorcolumn=88
    end
    " Maximum width of text that is being inserted
    " Longer lines will be broken after white space to get this width
    set textwidth=0
    " Copy indent from current line when starting a new line
        set autoindent
    " Do smart indenting when starting a new line
    " Only available when compiled with the +smartindent feature
        set smartindent
    " Number of spaces to use for each step of (auto)indent
        set shiftwidth=4
    " Use spaces instead of tab
        set expandtab
    " Number of spaces that a tab counts for
        set tabstop=4
    " Number of spaces that a tab counts for while performing editing operations
        set softtabstop=4
    " For tabs ^^^ see below about autocmd
    "
        set smarttab
        set shiftround
    " Number of pixel lines inserted between characters
    " Only in GUI
        set linespace=1
    " Highlight the screen line of the cursor
    " Only available when compiled with the +syntax feature
    " set to nocursorline becouse slowly scrolling at some big files
        set nocursorline
    " Turn off cursor blinking in normal mode
    " Only available when compiled with GUI enabled
        set guicursor=n:blinkon0
    " Remove all components and options of the GUI
    " Only available when compiled with GUI enabled
        set guioptions=
    " Number of colors
        set t_Co=256
    " Splitting a window will put the new window below the current one
    " Only available when compiled with the +windows feature
        set splitbelow
    " Splitting a window will put the new window right of the current one
    " See :vsp
    " Only available when compiled with the +vertsplit feature
        "set splitright
    " Don't show the intro message starting Vim
        set shortmess+=I
    " Turn mouse pointer to a up-down sizing arrow
    " Only available when compiled with the +mouseshape feature
        set mouseshape=s:udsizing,m:no
    " Hide the mouse when typing text
    " Only works in GUI
        set mousehide
    " Edit several files in the same time without having to save them
        set hidden
    " No beeps, no flashes
        set novisualbell
        set t_vb=
    " List of directories which will be searched when using :find, gf, etc.
    " Search relative to the directory of the current file
    " Search in the current directory
    " Search downwards in a directory tree
    " Only available when compiled with the +path_extra feature
        set path=.,,**
    " Font settings
        "set guifont=Courier\ New\ 14\ Pitch\ 14"
        "set guifont=Lucida_Console:h12:::
        "set guifont=Terminus\ Re33:h12:::
        set guifont=DejaVu\ Sans\ Mono\ Nerd\ Font\ 18"
        set guioptions-=T
        set guioptions-=m
        set display=lastline
    " Cyrillic support
        set keymap=russian-jcukenwin
        " default english
        set iminsert=0
        " default english
        set imsearch=0
        " hl for cyrillic
        highlight lCursor guifg=NONE guibg=Cyan
    " Syntax
        syntax on
    " Highlight marks
        "syn match   myTodo   contained   "\<\(TODO\|FIXME\|HACK\|REVIEW\):"
        "hi def link myTodo Todo
    " Status line
        " Status line settings in Airline plugin
        set statusline+=%{gutentags#statusline()}
        " set statusline+=%-10.3n\                     " buffer number

    " Folding
        " za = toggle current fold
        " zR = open all folds
        " zM = close all folds
        " From https://github.com/sjl/dotfiles/blob/master/vim/.vimrc
        "function! MyFoldText()
        "let line = getline(v:foldstart)
        "let nucolwidth = &fdc + &number * &numberwidth
        "let windowwidth = winwidth(0) - nucolwidth - 3
        "let foldedlinecount = v:foldend - v:foldstart
        "" expand tabs into spaces
        "let onetab = strpart(' ', 0, &tabstop)
        "let line = substitute(line, '\t', onetab, 'g')
        "let line = strpart(line, 0, windowwidth - 2 - len(foldedlinecount))
        "let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
        "return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
        "endfunction
        "set foldtext=MyFoldText()
        " Lines with equal indent form a fold
            set foldmethod=manual
        " Maximum nesting of folds
        " Only available when compiled with the +folding feature
            set foldnestmax=10
        " All folds are open
        " Only available when compiled with the +folding feature
            set nofoldenable
        " Folds with a higher level will be closed
        " Only available when compiled with the +folding feature
            set foldlevel=1
        " Remove the extrafills --------
        " Only available when compiled with the +windows and +folding features
            set fillchars="fold: "

    " Search
        " While typing a search command, show pattern matches as it is typed
        " Only available when compiled with the +extra_search feature
        set incsearch
        " When there is a previous search pattern, highlight all its matches
        " Only available when compiled with the +extra_search feature
        set hlsearch
        " Ignore case in search patterns
        set ignorecase
        " Override the 'ignorecase' if the search pattern contains upper case characters
        set smartcase
        " All matches in a line are substituted instead of one
        set gdefault
    " Diff settings
        " Ignore whitespaces in vimdiff
        set diffopt+=iwhite
        " Always let some lines before and after your cursor
        set scrolloff=8
    " Ограничить длину строки, при подсветке синтаксиса
    " Включаем, чтобы vim быстрее работал с файлами с длинной строкой,
    " например минифицированные файлы js
    set synmaxcol=300
    " При открытии буфера из location list или quickfix list файл будет
    " открываться либо в том окне где он уже открыт, либо в новом табе.
    " Это может быть удобно при рефакторинге, когда quickfix занят, и
    " открываем location list, и хотим чтобы он хранил результат, пока мы
    " работаем в другом буфере.
    " set switchbuf+=usetab,newtab
    " Terminal settings
        tnoremap <C-W>n <C-\><C-n>
    " Открыть в сплит окне справа Модуль, функцию, переменную и т.д., что
    " обычно просто открывается по ctrl+]
    "nmap <c-[> <c-w><c-]><c-k><c-w>H
    "
    " Не перерисовывать экран посреди макроса (для повышения производительности)
    set lazyredraw
    " Боковая колонка со свёрнутыми блоками
    set foldcolumn=1
    " Только для Neovim. Настройка incommand показывает в реальном времени,
    " какие изменения внесёт команда. Сейчас поддерживается только s, но даже
    " это невероятно полезно
    " set inccommand=nosplit
    "
    "set autochdir
    " Neovim, set system clipboard by default
    " set clipboard+=unnamedplus
    "
    " change current word (like ciw) but repeatable with dot . for the same next
    " word
    nnoremap <silent> c<Tab> :let @/=expand('<cword>')<cr>cgn

    " Для некоторых файлов Python с документацией отключалась подсветка
    " синтаксиса, установка maxmempattern позволяет избежать этого
    set maxmempattern=5000

" set leader key
let mapleader = ","

" Plugins settings
    " BufExplorer
        let g:bufExplorerShowNoName=1 " Show 'No Name' buffers.
        let g:bufExplorerSortBy='mru' " Sort by most recently used.
    " vim-session
        let g:session_autoload = 'no'
        let g:session_default_overwrite = 0
        let g:session_autosave = 'yes'
        let g:session_autosave_periodic = 15
        let g:session_default_to_last = 1
    " NERDTree
        nmap <Bs> :NERDTreeToggle<CR>
        nnoremap <leader>m :NERDTreeFind<CR>
        let NERDTreeShowBookmarks=1
        let NERDTreeChDirMode=2
        let NERDTreeQuitOnOpen=1
        let NERDTreeShowHidden=1
        let NERDTreeAutoDeleteBuffer=1
        let NERDTreeKeepTreeInNewTab=0
        " Disable display of the 'Bookmarks' label and 'Press ? for help' text
        let NERDTreeMinimalUI=1
        " Use arrows instead of + ~ chars when displaying directories
        let NERDTreeDirArrows=1
        let NERDTreeBookmarksFile= $HOME . '/.vim/.NERDTreeBookmarks'
        let NERDTreeIgnore=['^__pycache__$', '^.pytest_cache$', '^.mypy_cache$', '.retry$']
        let g:NERDTreeWinSize=50
    " netrw
        "let g:netrw_banner = 0
        "let g:netrw_liststyle = 3
        "let g:netrw_browse_split = 4
        "let g:netrw_altv = 1
        "let g:netrw_winsize = 25
        "augroup ProjectDrawer
            "autocmd!
            "autocmd VimEnter * :Vexplore
        "augroup END
    " NERDCommenter
        let g:NERDSpaceDelims = 1
        let g:NERDCompactSexyComs = 1
    " tpope/vim-commentary
        " yank and past visual before toggle comment
        vmap gy ygvgc
        " yank line before toggle comment
        nmap gy yygcc
    " Undotree
        nnoremap <F6> :UndotreeToggle<CR>
        let g:undotree_WindowLayout = 2
        let g:undotree_SetFocusWhenToggle = 1
        " let g:undotree_HighlightSyntaxAdd = "DiffAdd"
    " CtrlP
	set wildignore+=*/Twig/*,*.so,*.swp,*.zip     " Linux/MacOSX
        let g:ctrlp_max_files = 0 " Иначе ищет не все файлы
        let g:ctrlp_custom_ignore = '\v[\/](node_modules|root\/dist|cover_db)|(\.(swp|ico|git|svn)|(nytprof))
                            \|(\.(tox|mypy_cache))|venv.+$'
        let g:ctrlp_max_height = 30
        let g:ctrlp_show_hidden = 1
        let g:ctrlp_use_caching = 1
        let g:ctrlp_mruf_relative = 1
        " let g:ctrlp_clear_cache_on_exit = 0
        let g:ctrlp_extensions = [ 'buffertag', 'quickfix', 'undo', 'line', 'changes', 'bookmarkdir']
        nnoremap <leader>bc :CtrlPMRUFiles<CR>
        nnoremap <leader>bl :CtrlPLine<CR>

    " Vim wiki
        " Highlights settings
        hi VimwikiHeader1 ctermfg=9
        hi VimwikiHeader2 ctermfg=46
        hi VimwikiHeader3 ctermfg=19
        hi VimwikiHeader4 ctermfg=13
        hi VimwikiHeader5 ctermfg=14
        hi VimwikiHeader6 ctermfg=11
        let g:vimwiki_list = [{'path': '~/mysettings/vimwiki/'}]
                    " \{'path': '/opt/postgrespro/pgpro-manager/ee-manager-api/vimwiki/'}]

		" Настройки wiki (Нужно переместить в .local.vimrc)
		call extend(g:vimwiki_list, [
                    \ {'path': '/path/to/vimwiki/'},
                    \])
        nnoremap <M-t> :VimwikiToggleListItem<CR>
    " Tasklist
        let g:tlWindowPosition = 1
        let g:tlTokenList = ['TODO', 'FIXME', 'REVIEW', 'WTF', 'HACK', 'BUG']
    " vim-airline
        let g:airline_section_a = airline#section#create(['mode'])
        " unicode symbols
        let g:airline_symbols.crypt = '🔒'
        let g:airline_symbols.linenr = '☰'
        let g:airline_symbols.maxlinenr = ''
        let g:airline_symbols.paste = 'ρ'
        " let g:airline_symbols.paste = 'Þ'
        " let g:airline_symbols.paste = '∥'
        let g:airline_symbols.spell = 'Ꞩ'
        let g:airline_symbols.notexists = 'Ɇ'
        let g:airline_symbols.whitespace = 'Ξ'
        let g:airline_left_sep = ''
        let g:airline_left_alt_sep = ''
        let g:airline_right_sep = ''
        let g:airline_right_alt_sep = ''
        let g:airline_symbols.branch = ''
        let g:airline_symbols.readonly = ''
        let g:airline_symbols.dirty='⚡'
        let g:airline#extensions#hunks#enabled = 0

    " w0rp/ale
    " Set this. Airline will handle the rest.
        let g:airline#extensions#ale#enabled = 0
        let g:ale_sign_error = '>>'
        let g:ale_sign_warning = '--'
        let g:ale_perl_perl_options = '-c -Mwarnings -Ilib -It/lib'
        let g:ale_perl_perlcritic_showrules = 1
        " let g:ale_pattern_options = {
        "             \   '.*\.json$': {'ale_enabled': 0},
        "             \   '.*node_modules/.*\.js$': {'ale_enabled': 0},
        "             \   '.*dist/.*\.js$': {'ale_enabled': 0},
        "             \}
        " if no set linters, will use all available
        " let b:ale_linters = {
        "             \ 'javascript': ['eslint', 'prettier'],
        "             \ 'python': ['flake8', 'pylint'],
        "             \ }
        let g:ale_fixers = {
                    \'python': ['black', 'autopep8', 'isort'],
                    \'ruby': ['rubocop'],
                    \'perl': ['perltidy'],
                    \}
        let g:ale_python_flake8_options = '--max-line-length=88'
        let g:ale_python_pylint_change_directory=0
        let g:ale_python_flake8_change_directory=0
        let g:ale_javascript_prettier_options = '--single-quote --trailing-comma all "printWidth": 120'
        let g:ale_sign_column_always = 1
        nmap <F8> <Plug>(ale_fix)
        nmap <silent> <leader>aj :ALENext<cr>
        nmap <silent> <leader>ak :ALEPrevious<cr>

    "Prettier
    " autocmd TextChanged,InsertLeave *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
    nnoremap <F2> :PrettierAsync<CR>

    " Tags
        " taglist.vim.git
            let Tlist_GainFocus_On_ToggleOpen = 1
            let Tlist_Close_On_Select = 1
            let Tlist_WinWidth = 50
            " let Tlist_Sort_Type = "name"
        " vim-autotag.git
            let g:autotagTagsFile=".tags"
    " gutentags
        "let g:gutentags_add_default_project_roots = 0
        "let g:gutentags_project_root = ['package.json', '.git']
        "let g:gutentags_trace = 1
        "
        " Чтобы использовать только те тэги, которые действительно нужны.
        " Не нужные файлы исключаются в конфигурации ripgrep (см. RIPGREP_CONFIG_PATH
        " https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md#configuration-file)
        let g:gutentags_file_list_command = 'rg --files'
    " dbtext
        let g:dbext_default_profile_mySQLProfile = 'type=MYSQL:user=db_user:passwd=db_user_pass:dbname=dbname:host=127.0.0.1:port=3307'
        augroup Profile
            au!
            " Automatically choose the correct dbext profile
            autocmd BufRead /home/user/project/* DBSetOption profile=mySQLProfile
        augroup end
    " Tabular
        " Выравнить только по первому вхождению в строке
        " autocmd VimEnter AddTabularPattern! 1= /^[^=]*\zs=
        cnoreabbrev Tab Tabularize

    "
    " YouCompleteMe
    let g:ycm_semantic_triggers =  {
                \   'c': ['->', '.'],
                \   'objc': ['->', '.', 're!\[[_a-zA-Z]+\w*\s', 're!^\s*[^\W\d]\w*\s',
                \            're!\[.*\]\s'],
                \   'ocaml': ['.', '#'],
                \   'cpp,cuda,objcpp': ['->', '.', '::'],
                \   'perl': ['->', '::'],
                \   'php': ['->', '::'],
                \   'cs,d,elixir,go,groovy,java,javascript,juliaerl6ython,scala,typescript,vb': ['.'],
                \   'ruby,rust': ['.', '::'],
                \   'lua': ['.', ':'],
                \   'erlang': [':'],
                \ }
    let g:ycm_server_log_level = 'critical'
    " Plugin 'shime/vim-livedown'
		" should markdown preview get shown automatically upon opening markdown buffer
            let g:livedown_autorun = 1
		" should the browser window pop-up upon previewing
            let g:livedown_open = 0
		" the port on which Livedown server will run
            let g:livedown_port = 1337
		" the browser to use
            "let g:livedown_browser = "safari"
    " https://github.com/gu-fan/riv.vim
        " Включаем подсветку синтаксиса для reSTructeredText в docstrings
        let g:riv_python_rst_hl=1


    " " Plugin Shougo/unite
		" " Автоматический insert mode
		" let g:unite_enable_start_insert = 1

		" " Отображаем Unite в нижней части экрана
		" let g:unite_split_rule = "botright"

		" " Отключаем замену статус строки
		" let g:unite_force_overwrite_statusline = 0

		" " Размер окна Unite
		" let g:unite_winheight = 10

		" " Красивые стрелочки
		" let g:unite_candidate_icon="▷"
        " "nnoremap <leader>f :<C-u>Unite -buffer-name=files -start-insert buffer file_rec/async:!<cr>
    " Plugin jaxbot/browserlink
        let g:bl_no_mappings = 1
    " Plugin tveskag/nvim-blame-line
        " nmap <silent> <leader>bl :ToggleBlameLine<CR>
    " Plugin NeoTags
        let g:neotags_verbose = 0
    " Plugin Vim-signature
      let g:SignatureMap = {
        \ 'ListGlobalMarks'    :  "m//",
        \ }
    " Plugin Yggdroot/indentLine.git
        " ei - extended ... indentlinestoggle
        nnoremap <leader>ei :IndentLinesToggle<CR>
        "let g:indentLine_setColors = 0
    " vim-tmux-navigator
        " Отключить поведение ctrl+j по умолчанию
        let g:Perl_Ctrl_j = 'off'
    "" Taboo
        "let g:taboo_tab_format = ' %r%f%m '
    " Local directory for tab.
    " Важно! Должен быть установлен плагин https://github.com/thinca/vim-localrc.git
    " и в каждой директории проекта, который мы хотим подключить должен быть
    " файл .local.vimrc, с такой переменной, для каждого проекта путь свой
    " let t:wd = "/opt/regru/srs"
        au TabEnter,BufRead * if exists("t:wd") | exe "cd" t:wd | endif
    " Vdebug
    "let g:vdebug_options = {}
    "let g:vdebug_options['server'] = '172.16.33.33'
    "let g:vdebug_options['port'] = '9000'

    " Neoterm
    let g:neoterm_default_mod='vertical rightbelow' " open terminal in right split
    "let g:neoterm_size=100 " terminal split size
    let g:neoterm_autoscroll=1 " scroll to the bottom when running a command
    "let g:neoterm_autoinsert = 1
    " autocmd TermOpen * silent! lcd %:p:h

    " Startify
    let g:startify_session_dir = '~/.vim/sessions'
    let g:startify_lists = [
                \ { 'type': 'sessions',  'header': ['   Sessions']       },
                \ { 'type': 'files',     'header': ['   MRU']            },
                \ ]
    " ncm2/float-preview
    "let g:float_preview#docked = 1
    " YankRing
        " let g:yankring_replace_n_pkey = '<M-p>'
        " let g:yankring_replace_n_nkey = '<M-P>'

    " vim-yankstack.git
    " No using default keymaps to avoid mapping conflict
        let g:yankstack_map_keys = 0
        let g:yankstack_yank_keys = ['c', 'C', 'd', 'D', 's', 'x', 'X', 'y', 'Y']
		nmap <leader>p <Plug>yankstack_substitute_older_paste
		nmap <leader>P <Plug>yankstack_substitute_newer_paste

    " svermeulen/vim-yoink.git
        " " It doesn't work in mac os
        " nmap p <plug>(YoinkPaste_p)
        " nmap P <plug>(YoinkPaste_P)
        " xmap p <plug>(SubversiveSubstitute)
        " xmap P <plug>(SubversiveSubstitute)
        " nmap <a-n> <plug>(YoinkPostPasteSwapBack)
        " nmap <a-p> <plug>(YoinkPostPasteSwapForward)
        "" let g:ctrlp_map=''
        "" nmap <expr> <c-p> yoink#canSwap() ? '<plug>(YoinkPostPasteSwapForward)' : '<Plug>(ctrlp)'
        "" nmap <expr> P yoink#canSwap() ? '<plug>(YoinkPostPasteSwapForward)' : '<plug>(YoinkPaste_P)'


    "vim-colorscheme-switcher
        "let g:colorscheme_switcher_define_mappings=0
        "let g:colorscheme_switcher_exclude_builtins=1
        " let g:colorscheme_switcher_exclude = ['blue', 'delek', 'evening', 'morning', 'pablo', 'shine', 'calmar256-dark', 'darkblue', 'industry', 'murphy', 'peachpuff', 'calmar256-light', 'default', 'elflord', 'koehler', 'native', 'ron', 'torte', 'slate', 'zellner']
        " crt, desert
        "
    " vim-python/python-syntax
    let g:python_highlight_all = 1

    " vim-table-mode
    " let g:table_mode_tableize_map = ""
	" markdown compatible
	let g:table_mode_corner='|'

    " ********************** Colorscheme settings *******************************
    set termguicolors     " enable true colors support

    " colorscheme ayu-vim
    " let ayucolor="light"  " for light version of theme
    " let ayucolor="mirage" " for mirage version of theme
    " let ayucolor="dark"   " for dark version of theme

    " colorscheme one
    " let g:airline_theme='one'

    " ***************************************************************************

    " Vim-dispatch
    " Для нужного проекта в файл .local.vimrc можно добавить нужную команду
    " autocmd FileType perl let b:dispatch = 'docker exec -ti backoffice prove -vs /www/srs/%'
    " autocmd FileType python let b:dispatch = 'docker exec -ti subscribers python manage.py tests -p %'
    " тесты запускаются командой `<CR>
    " подробности см. в документации vim-dispatch
    " Ripgrep
    let g:rg_command = 'rg --vimgrep --pcre2 --hidden -S'
    " SplitJoin
    let g:splitjoin_align = 1
    let g:splitjoin_perl_hanging_args = 0
    " Vim-argwrap
    nnoremap <silent> <leader>a :ArgWrap<CR>
    " Tagbar
    let g:airline#extensions#tagbar#flags = 'f'  " show full tag hierarchy
    let g:tagbar_position = 'topleft vertical'
    let g:tagbar_width = 50
    let g:tagbar_autoclose = 1
    let g:tagbar_autofocus = 1
    " SirVer/ultisnips
    let g:UltiSnipsExpandTrigger="<c-l>"


    " airblade/vim-gitgutter
    command! Gqf GitGutterQuickFix | copen

    " github.com/tpope/vim-fugitive.git
    " alias for Git blamd command
    command! Gblame Git blame
	autocmd FileType fugitiveblame nmap <buffer> q <C-W>q

	" https://github.com/junegunn/vim-easy-align
	" Start interactive EasyAlign in visual mode (e.g. vipga)
	xmap ga <Plug>(EasyAlign)

	" Start interactive EasyAlign for a motion/text object (e.g. gaip)
	nmap ga <Plug>(EasyAlign)

    " vim-fuzzysearch
    nnoremap f/ :FuzzySearch<CR>
    let g:fuzzysearch_prompt = 'fuzzy /'
    let g:fuzzysearch_hlsearch = 1
    let g:fuzzysearch_ignorecase = 1
    let g:fuzzysearch_max_history = 30
    let g:fuzzysearch_match_spaces = 0


" Шорткаты

    " Navigate with <Ctrl>-hjkl in Insert mode
        imap <C-h> <C-o>h
        imap <C-j> <C-o>j
        imap <C-k> <C-o>k
        imap <C-l> <C-o>l
        imap <C-w> <C-o>w
    " Switch splits
        "nmap <C-h> <C-W>h
        "nmap <C-j> <C-W>j
        "nmap <C-k> <C-W>k
        "nmap <C-l> <C-W>l
    " ,v
        " Open the .vimrc in a new tab
        nmap <leader>v :tabedit $MYVIMRC<CR>
        :cabbrev e NERDTreeClose<CR>:e!
    " <Space> = <PageDown>
        nmap <Space> <PageDown>
    " n и N
        " Search matches are always in center
        nmap n nzz
        nmap N Nzz
        nmap * *zz
        nmap # #zz
        nmap g* g*zz
        nmap g# g#zz
    " Navigate through wrapped lines
        noremap j gj
        noremap k gk
    " gf
        " Open file under cursor in a new vertical split
        nmap gf :vertical wincmd f<CR>
    " ,ts
        " Fix trailing white space
        map <leader>ts :%s/\s\+$//e<CR>
    " ,p
        " Toggle the 'paste' option
        "set pastetoggle=<Leader>p
    " В коммандном режиме разрешить прыгать в конец и начало строки,
    " как в консоли
        cnoremap <c-e> <end>
        imap     <c-e> <c-o>$
        cnoremap <c-a> <home>
        imap     <c-a> <c-o>^
    " {<CR>
        " auto complete {} indent and position the cursor in the middle line
        inoremap {<CR> {<CR>}<Esc>O
        inoremap (<CR> (<CR>)<Esc>O
        inoremap [<CR> [<CR>]<Esc>O
    " Switch tabs with <Tab>
        " conflict with ctrl+i (tab)
        "nmap <Tab> gt
        "nmap <S-Tab> gT
        " Go to tab by number
            noremap <leader>1 1gt
            noremap <leader>2 2gt
            noremap <leader>3 3gt
            noremap <leader>4 4gt
            noremap <leader>5 5gt
            noremap <leader>6 6gt
            noremap <leader>7 7gt
            noremap <leader>8 8gt
            noremap <leader>9 9gt
            noremap <leader>0 :tablast<cr>
        " Go to last active tab
            au TabLeave * let g:lasttab = tabpagenr()
            nnoremap <silent> <leader>l :exe "tabn ".g:lasttab<cr>
            vnoremap <silent> <leader>l :exe "tabn ".g:lasttab<cr>
    " Myself shorcut
        " nnoremap <F7> :TlistToggle<CR>
        nmap <F7> :TagbarToggle<CR>
        "nnoremap <F4> :OpenSession<CR>
        nnoremap <F5> :!%:p<CR>

        " Search visual selected text
        vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>
    " Copy (yank) current filename to clipboard
        nnoremap cp :let @" = expand("%:t")<CR>
    " Yank and write selected text to file
    " actual for using with Windows Putty
        vmap <C-c> :'<,'>w! ~/clipboard.txt<CR>
    " Close buffer smarty (https://u.to/WHsIGA)
        cnoremap sbd :b#<bar>bd#
    " Scroll window per line
        "let g:BASH_Ctrl_j = 'off'
        "let g:BASH_Ctrl_k = 'off'
        nnoremap <M-k> <C-y>
        nnoremap <M-j> <C-e>
    " Put to the register, last editted line between files ( and more here
    " https://clck.ru/N5Kog)
    autocmd InsertLeave * execute 'normal! mI'

    " Zoom current buffer
    nnoremap <C-w>o <C-w>\|
    nnoremap <C-w>oo <C-w><C-w><C-w>\|

    " PerlTidy https://stackoverflow.com/a/8347351
    "define :Tidy command to run perltidy on visual selection || entire buffer"
    command -range=% -nargs=* Tidy <line1>,<line2>!perltidy

    "run :Tidy on entire buffer and return cursor to (approximate) original position"
    fun DoTidy()
        let l = line(".")
        let c = col(".")
        :Tidy
        call cursor(l, c)
    endfun

    "shortcut for normal mode to run on entire buffer then return to current line"
    autocmd Filetype perl nmap <C-F6> :call DoTidy()<CR>

    "shortcut for visual mode to run on the current visual selection"
    autocmd Filetype perl vmap <C-F6> :Tidy<CR>

    " Search command
    " command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
    " nnoremap <silent> <Leader>f :Rg<CR>

" Environment
    " Store lots of history entries: :cmdline and search patterns
    set history=1000
    " Backspacing settings
        " start     allow backspacing over the start of insert;
        "           CTRL-W and CTRL-U stop once at the start of insert.
        " indent    allow backspacing over autoindent
        " eol       allow backspacing over line breaks (join lines)
        set backspace=indent,eol,start
    " Backup и swp files
        " Don't create backups
        set nobackup
        " Don't create swap files
        set noswapfile
    " Undo settings
        if version >= 700
            set history=64
            set undolevels=128
            set undodir=~/.vim/undodir/
            set undofile
            set undolevels=1000
            set undoreload=10000
        endif
    " grepprg
        set grepprg=ack
        " let g:ackprg = 'ag --nogroup --nocolor --column'
        " let g:ackprg = 'ag --vimgrep --smart-case'
        " let g:ackprg = "ack-grep -H"
    " sessions
        set sessionoptions-=blank,help
        " Настройки для Taboo
        set sessionoptions+=tabpages,globals
        " Save folding in sessions
        set sessionoptions+=folds
    " line numbers
        let g:relativenumber = 0
	set number
    " keyword symbols
        set iskeyword=@,48-57,_,192-255,$,%,64,-
        set iskeyword-=:

" File specific
    autocmd BufNewFile,BufRead *.{tmpl,tt,tt2,inc} set filetype=html
    autocmd BufNewFile,BufRead *.t set filetype=perl
    set fileformats=unix,dos
    autocmd FileType pug setlocal tabstop=2 softtabstop=2 shiftwidth=2
    autocmd FileType perl set iskeyword-=-
    autocmd FileType yaml set iskeyword-=:
    autocmd FileType javascript set iskeyword-=:
    autocmd FileType python set iskeyword-=:
    autocmd FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2
    autocmd FileType yaml set foldmethod=indent foldnestmax=2
    " Включаем для python автоматическое добавление символов комментария для новых
    autocmd FileType python set formatoptions+=ro
    autocmd FileType python set foldmethod=indent foldnestmax=2
    " Включаем для sh автоматическое добавление символов комментария для новых
    autocmd FileType sh set formatoptions+=ro
    autocmd FileType vimwiki setlocal expandtab

" Compiler settings
    autocmd BufNewFile,BufRead *.pl compiler perl

" Compiler settings
    autocmd BufWinEnter quickfix set cursorline

" Colorscheme
    " colorscheme crt
    set bg=dark
    " colorscheme solarized
    colorscheme darkblue

    hi TabLine term=underline cterm=underline ctermfg=20 ctermbg=0 gui=underline guibg=Black
    hi TabLineSel term=underline,reverse cterm=underline,reverse ctermfg=Black ctermbg=10 gui=bold


    " let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    " let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    " Comment if use native Vim
    "autocmd TermEnter * colorscheme desert
    "function SetColorscheme()
    "    IndentLinesDisable
    "    colorscheme crt
    "    IndentLinesEnable
    "    "set bg=dark
    "    "syntax sync fromstart
    "endfunction
    "autocmd TermLeave * call SetColorscheme()


" My users commands
    "command JSONPrettyPrintEntireFile %!python -m json.tool
    " Делаем два раза json.dumps чтобы получить экранирование кавычек, иначе нетривиальная задача для vim + python
    "command JSONEntireFileToString %!python -c "import json, sys; d=sys.stdin; obj=json.load(d); str=json.dumps(obj); print(json.dumps(str));"
    "
    function! JsonEscape() range
        execute a:firstline . ',' . a:lastline . '!jq -R'
    endfunction
    command! -range=% JsonEscape <line1>,<line2>call JsonEscape()

" Don't allow editing of read only files
    autocmd BufRead * call RONoEdit()

    function! RONoEdit()
        if &readonly == 1
            set nomodifiable
        else
            set modifiable
        endif
    endfunction

" Rebind Alt key for GNOME terminal
" src https://stackoverflow.com/questions/6778961/alt-key-shortcuts-not-working-on-gnome-terminal-with-vim
let c='a'
while c <= 'z'
  exec "set <A-".c.">=\e".c
  exec "imap \e".c." <A-".c.">"
  let c = nr2char(1+char2nr(c))
endw

set timeout ttimeoutlen=50

" Функция для проверки наличия ключа и значения в списке словарей
" Используется для подключения вики некоторого проекта
function Match_dict_in_array(mylist, dict_key, dict_value)
    for item in a:mylist
        if has_key(l:item, a:dict_key) && l:item[a:dict_key] == a:dict_value
            return 1
        endif
    endfor
    return 0
endfunction

" Биндим ресайз окошек на + и -
if bufwinnr(1)
  map + <C-W>+
  map - <C-W>-
endif
